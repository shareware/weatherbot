package icons

const (
	Calendar    = "📅"
	Thermometer = "🌡"
	Alarm       = "⏰"
	Pin         = "📌"
	Bucket      = "🗑"
	Pencil      = "✏️"
	Check       = "✅"
	SunClock    = "⌛"
	Back        = "🔙"

	Sun                = "☀️"
	Cloudy             = "🌤"
	Owercast           = "☁️"
	CloudyAndLightRain = "🌦"
	CloudyAndRain      = "🌧"
	CloudyAndLightSnow = "🌨"
	Snow               = "❄️"
	Wind               = "💨"
)
