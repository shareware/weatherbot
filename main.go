package main

import (
	"errors"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/weathersender/icons"
	"github.com/weathersender/tgkeyboard"
	"github.com/weathersender/weatherapi"

	"github.com/Syfaro/telegram-bot-api"
)

var (
	users = make(map[int64]*user)
)

const (
	botKey = "602894391:AAHfLtuDISOQ7YakAqdh6ONVlAt1l4NMNVY"

	helloMsg = "*Привет! Я умею:*\n" +
		icons.Thermometer + " _Отображать текущую погоду_\n" +
		icons.Calendar + " _Отображать погоду на 10 дней_\n" +
		icons.Alarm + " _Отсылать вам погоду в указаный час каждый день_"

	weatherWeekCommand = "На"
	weatherNowCommand  = "Сейчас"
	alarmCommand       = "Оповещения"
	newAlarmCommand    = "Новое"
	deleteAlarmCommand = "Удалить"
	backCommand        = "Назад"
)

func initUsers(c *chan int64) {
	db, err := getDbConnect()
	if err != nil {
		db.Close()
		fmt.Println(err.Error())
	}

	rows, err := db.Query("select * from tgweather.Users")
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer rows.Close()
	for rows.Next() {
		u := &user{}
		var currentAlarm string
		err = rows.Scan(&u.chatID, &currentAlarm)
		if err != nil {
			continue
		}
		u.lvl = []string{"main"}

		if currentAlarm != "" {
			duration, err := getUserTimeDuration(currentAlarm)
			if err != nil {
				fmt.Println(err.Error())
				continue
			}
			u.setAlarm(c, duration)
		}
		users[u.chatID] = u
	}

}

func timerCheker(bot *tgbotapi.BotAPI, c *chan int64) {
	for {
		select {
		case msg := <-*c:
			var send tgbotapi.MessageConfig
			out := weatherapi.GetWeatherToday()
			if out != "" {
				send = tgbotapi.NewMessage(msg, out)
			} else {
				send = tgbotapi.NewMessage(msg, "Не удалось получить информцию о погоде")
			}
			send.ParseMode = tgbotapi.ModeMarkdown
			bot.Send(send)
		}
	}
}

func validateUserAlarm(t string) (string, error) {
	timeArr := strings.Split(t, ":")
	if len(timeArr) == 1 || timeArr == nil {
		return "", errors.New("указан неверный разделитель")
	}
	hour, err := strconv.Atoi(timeArr[0])
	min, err2 := strconv.Atoi(timeArr[1])
	if err != nil || err2 != nil || hour < 0 || hour > 23 || min < 0 || min > 59 {
		return "", errors.New("неверный формат времени")
	}
	for i, times := range timeArr {
		if len(times) == 1 {
			timeArr[i] = "0" + times
		}
	}

	return strings.Join(timeArr, ":"), nil

}

func getUserTimeDuration(t string) (time.Duration, error) {

	_, err := validateUserAlarm(t)
	if err != nil {
		return time.Duration(0), err
	}

	timeArr := strings.Split(t, ":")

	hour, _ := strconv.Atoi(timeArr[0])
	min, _ := strconv.Atoi(timeArr[1])

	t3 := time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), time.Now().Hour(), time.Now().Minute(), time.Now().Second(), time.Now().Nanosecond(), time.UTC)
	t4 := time.Date(time.Now().Year(), time.June, time.Now().Day(), hour, min, 0, 0, time.UTC)
	return t4.Sub(t3), nil
}

func main() {

	mainChan := make(chan int64)
	bot, err := tgbotapi.NewBotAPI(botKey)
	if err != nil {
		log.Panic(err)
	}
	bot.Debug = true
	go timerCheker(bot, &mainChan)

	log.Printf("Authorized on account %s", bot.Self.UserName)
	initDB()
	initUsers(&mainChan)
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, _ := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.CallbackQuery != nil {
			msg := tgbotapi.EditMessageReplyMarkupConfig{}
			newmark := tgbotapi.InlineKeyboardMarkup{}
			msgcon := tgbotapi.CallbackConfig{}
			if users[update.CallbackQuery.Message.Chat.ID].getCurentLvl() == deleteAlarmCommand {
				users[update.CallbackQuery.Message.Chat.ID].deleteAlarm(update.CallbackQuery.Data)
				newmark = tgkeyboard.GetInlineTimeKeyboard(users[update.CallbackQuery.Message.Chat.ID].getCurrentAlarm(), users[update.CallbackQuery.Message.Chat.ID].getAlarms())
				msgcon = tgbotapi.NewCallback(update.CallbackQuery.ID, icons.Bucket)
			} else {
				newmark = tgkeyboard.GetInlineTimeKeyboard(update.CallbackQuery.Data, users[update.CallbackQuery.Message.Chat.ID].getAlarms())
				msgcon = tgbotapi.NewCallback(update.CallbackQuery.ID, icons.Check)

				if users[update.CallbackQuery.Message.Chat.ID].getCurrentAlarm() == strings.Split(update.CallbackQuery.Data, " ")[0] {
					users[update.CallbackQuery.Message.Chat.ID].stopAlarm()
				} else {
					newAlarmDuration, err := getUserTimeDuration(update.CallbackQuery.Data)
					if err != nil {
						log.Fatal(err.Error())
						continue
					}
					users[update.CallbackQuery.Message.Chat.ID].stopAlarm()
					users[update.CallbackQuery.Message.Chat.ID].setCurrentAlarm(update.CallbackQuery.Data)
					users[update.CallbackQuery.Message.Chat.ID].setAlarm(&mainChan, newAlarmDuration)
				}

			}
			msg.ReplyMarkup = &newmark
			msg.MessageID = update.CallbackQuery.Message.MessageID
			msg.ChatID = update.CallbackQuery.Message.Chat.ID
			bot.AnswerCallbackQuery(msgcon)
			bot.Send(msg)

		}
		if update.Message == nil {
			continue
		}
		command := strings.Split(update.Message.Text, " ")
		commands(command, update, bot, mainChan)

		log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)
	}
}

func commands(command []string, update tgbotapi.Update, bot *tgbotapi.BotAPI, c chan int64) {
	switch command[0] {
	case "/start":
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, helloMsg)
		msg.ParseMode = tgbotapi.ModeMarkdown
		u := users[update.Message.Chat.ID]
		if u == nil {
			users[update.Message.Chat.ID] = newUser(update.Message.Chat.ID)
			err := initNewUserInDb(update.Message.Chat.ID)
			if err != nil {
				fmt.Println(err.Error())
			}
		}

		msg.ReplyMarkup = tgkeyboard.GetMainKeyboard()
		bot.Send(msg)
	case alarmCommand:
		msg := tgbotapi.MessageConfig{}

		users[update.Message.Chat.ID].nextLvl(alarmCommand)

		msg = tgbotapi.NewMessage(update.Message.Chat.ID, "_Выберите одно из существующих оповещений или добавьте свое_")
		msg.ReplyMarkup = tgkeyboard.GetAlarmKeyboard()
		msg.ParseMode = tgbotapi.ModeMarkdown
		bot.Send(msg)

		msg = tgbotapi.NewMessage(update.Message.Chat.ID, "*Список оповещений:*")
		msg.ReplyMarkup = tgkeyboard.GetInlineTimeKeyboard(users[update.Message.Chat.ID].getCurrentAlarm(), users[update.Message.Chat.ID].getAlarms())
		msg.ParseMode = tgbotapi.ModeMarkdown
		bot.Send(msg)
	case weatherNowCommand:
		out := weatherapi.GetWeatherNow()
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, out)
		msg.ParseMode = tgbotapi.ModeMarkdown
		bot.Send(msg)
	case newAlarmCommand:
		users[update.Message.Chat.ID].nextLvl(newAlarmCommand)

		msg := tgbotapi.NewMessage(update.Message.Chat.ID, "_Оправьте мне время в формате_ *чч:мм*")
		msg.ReplyMarkup = tgkeyboard.GetBackKeyboard()
		msg.ParseMode = tgbotapi.ModeMarkdown
		bot.Send(msg)
	case deleteAlarmCommand:

		users[update.Message.Chat.ID].nextLvl(deleteAlarmCommand)
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, "_Выберите оповещение которое хотите удалить_")
		msg.ReplyMarkup = tgkeyboard.GetBackKeyboard()
		msg.ParseMode = tgbotapi.ModeMarkdown
		bot.Send(msg)
		msg = tgbotapi.NewMessage(update.Message.Chat.ID, "*Список оповещений:*")
		msg.ReplyMarkup = tgkeyboard.GetInlineTimeKeyboard(users[update.Message.Chat.ID].getCurrentAlarm(), users[update.Message.Chat.ID].getAlarms())
		msg.ParseMode = tgbotapi.ModeMarkdown
		bot.Send(msg)

	case backCommand:
		users[update.Message.Chat.ID].prevLvl()
		commands([]string{users[update.Message.Chat.ID].getCurentLvl()}, update, bot, c)
	case weatherWeekCommand:
		out := weatherapi.GetWeatherTenDays()
		for _, day := range out {
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, day)
			msg.ParseMode = tgbotapi.ModeMarkdown
			bot.Send(msg)
		}

	case "main":
		if users[update.Message.Chat.ID].getCurentLvl() != "main" {
			users[update.Message.Chat.ID].setLvl("main")
		}
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, helloMsg)
		msg.ReplyMarkup = tgkeyboard.GetMainKeyboard()
		msg.ParseMode = tgbotapi.ModeMarkdown
		bot.Send(msg)

	default:
		if users[update.Message.Chat.ID].getCurentLvl() == newAlarmCommand {
			validTime, err := validateUserAlarm(update.Message.Text)
			if err != nil {
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, err.Error())
				bot.Send(msg)
				commands([]string{users[update.Message.Chat.ID].getCurentLvl()}, update, bot, c)
				return
			}
			users[update.Message.Chat.ID].addAlarm(validTime)
			users[update.Message.Chat.ID].prevLvl()
			commands([]string{users[update.Message.Chat.ID].getCurentLvl()}, update, bot, c)
			return
		}

	}
}
