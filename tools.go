package main

func contains(slice []string, value string) bool {
	for _, v := range slice {
		if v == value {
			return true
		}
	}
	return false
}

func containsSlices(slices [][]string, val string) bool {
	for _, s := range slices {
		for _, v := range s {
			if v == val {
				return true
			}
		}

	}
	return false
}
