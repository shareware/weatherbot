package main

import (
	"fmt"
	"time"
)

type user struct {
	chatID int64
	lvl    []string
	timer  *time.Timer
}

func (u *user) getCurrentAlarm() string {
	currentAlarm, err := dbGetCurrentAlarm(u.chatID)
	if err != nil {
		fmt.Println(err.Error())
		return ""
	}
	return currentAlarm

}

func (u *user) setCurrentAlarm(alarm string) {
	err := dbSetCurrentAlarm(u.chatID, alarm)
	if err != nil {
		fmt.Println(err.Error())
	}
}

func (u *user) runTimer(c *chan int64) {
	<-u.timer.C
	*c <- u.chatID
}

func (u *user) stopAlarm() {
	currentAlarm, err := dbGetCurrentAlarm(u.chatID)
	if err != nil {
		fmt.Println(err.Error())
	}
	if currentAlarm != "" {
		u.timer.Stop()
	}
	err = dbSetCurrentAlarm(u.chatID, "")
	if err != nil {
		fmt.Println(err.Error())
	}

}

func (u *user) setAlarm(c *chan int64, d time.Duration) {
	u.timer = time.NewTimer(d)
	go u.runTimer(c)
}

func (u *user) addAlarm(alarm string) {
	alarms, err := dbGetAlarms(u.chatID)
	if err != nil {
		fmt.Println(err.Error())
	}
	if containsSlices(alarms, alarm) {
		return
	}
	addUserAlarmToDb(u.chatID, alarm)

}

func (u *user) getAlarms() [][]string {

	alarms, err := dbGetAlarms(u.chatID)
	if err != nil {
		fmt.Println(err.Error())
	}
	return alarms
}

func newUser(ID int64) *user {
	return &user{lvl: []string{"main"}, chatID: ID}
}

func (u *user) deleteAlarm(alarm string) {
	err := dbDeleteAlarm(u.chatID, alarm)
	if err != nil {
		fmt.Println(err.Error())
	}
}

func (u *user) prevLvl() string {
	if u.lvl == nil || len(u.lvl) == 0 {
		u.lvl = append(u.lvl, "main")
	}
	if len(u.lvl) > 1 {
		u.lvl = u.lvl[:len(u.lvl)-1]
	}
	return u.lvl[len(u.lvl)-1]
}

func (u *user) nextLvl(down string) {
	if u.getCurentLvl() != down && contains(u.lvl, down) == false {
		u.lvl = append(u.lvl, down)
	}

}

func (u *user) getCurentLvl() string {
	return u.lvl[len(u.lvl)-1]
}

func (u *user) setLvl(lvl string) {
	u.lvl = []string{lvl}
}
