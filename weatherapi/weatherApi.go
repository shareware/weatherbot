package weatherapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/weathersender/icons"
)

var (
	conditions = map[string][3]string{
		"clear":                            {"ясно", icons.Sun},
		"partly-cloudy":                    {"малооблачно", icons.Cloudy},
		"cloudy":                           {"облачно с прояснениями", icons.Cloudy},
		"overcast":                         {"пасмурно", icons.Owercast},
		"partly-cloudy-and-light-rain":     {"небольшой дождь", icons.CloudyAndLightRain},
		"partly-cloudy-and-rain":           {"дождь", icons.CloudyAndRain},
		"overcast-and-rain":                {"сильный дождь", icons.CloudyAndRain},
		"overcast-thunderstorms-with-rain": {"сильный дождь, гроза", icons.CloudyAndLightRain},
		"cloudy-and-light-rain":            {"небольшой дождь", icons.CloudyAndLightRain},
		"overcast-and-light-rain":          {"небольшой дождь", icons.CloudyAndLightRain},
		"cloudy-and-rain":                  {"дождь", icons.CloudyAndRain},
		"overcast-and-wet-snow":            {"дождь со снегом", icons.CloudyAndLightSnow},
		"partly-cloudy-and-light-snow":     {"небольшой снег", icons.CloudyAndLightSnow},
		"partly-cloudy-and-snow":           {"снег", icons.Snow},
		"overcast-and-snow":                {"снегопад", icons.Snow},
		"cloudy-and-light-snow":            {"небольшой снег", icons.CloudyAndLightSnow},
		"overcast-and-light-snow":          {"небольшой снег", icons.CloudyAndLightSnow},
		"cloudy-and-snow":                  {"снег", icons.Snow},
	}

	months = map[string]string{
		"January":   "Января",
		"February":  "Февраля",
		"March":     "Марта",
		"April":     "Апреля",
		"May":       "Мая",
		"June":      "Июня",
		"July":      "Июля",
		"August":    "Августа",
		"September": "Сентября",
		"October":   "Октября",
		"November":  "Ноября",
		"December":  "Декабря",
	}
	monthsDate = map[string]string{
		"01": "Января",
		"02": "Февраля",
		"03": "Марта",
		"04": "Апреля",
		"05": "Мая",
		"06": "Июня",
		"07": "Июля",
		"08": "Августа",
		"09": "Сентября",
		"10": "Октября",
		"11": "Ноября",
		"12": "Декабря",
	}
)

const (
	weatherKey = "4435d570-d7da-4314-a10f-4ebdb93e37de"
)

type weatherDayParams struct {
	FeelsLike int     `json:"feels_like"`
	Condition string  `json:"condition"`
	WindSpeed float64 `json:"wind_speed"`
}

type weather struct {
	Now  int              `json:"now"`
	Fact weatherDayParams `json:"fact"`

	Forecasts []struct {
		Date  string `json:"date"`
		Parts struct {
			Day   weatherDayParams `json:"day_short"`
			Night weatherDayParams `json:"night_short"`
		} `json:"parts"`
	} `json:"forecasts"`
}

func getWeather(days string) (weather, error) {
	req, _ := http.NewRequest("GET", "https://api.weather.yandex.ru/v1/forecast?lat=56.838607&lon=60.605514&limit="+days, nil)
	req.Header.Set("X-Yandex-API-Key", weatherKey)
	client := http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		return weather{}, err
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)
	w := weather{}

	if json.Unmarshal(body, &w); err != nil {
		return weather{}, err
	}
	return w, nil
}

//GetWeatherNow Возвращает погоду на текущий момент
func GetWeatherNow() string {
	w, err := getWeather("2")
	if err != nil {
		fmt.Println(err.Error())
		return ""
	}
	_, month, day := time.Now().Date()

	outString := "*Погода на " + strconv.Itoa(day) + " " + months[month.String()] + "*\n" +
		icons.Thermometer + " " + strconv.Itoa(w.Fact.FeelsLike) + " ℃" + "\n" +
		conditions[w.Fact.Condition][1] + " " + conditions[w.Fact.Condition][0] + "\n" +
		icons.Wind + " " + strconv.Itoa(int(w.Fact.WindSpeed)) + " м/с"
	return outString

}

// Возвращает погоду на сегодняшний день
func GetWeatherToday() string {
	w, err := getWeather("2")
	if err != nil {
		fmt.Println(err.Error())
		return ""
	}
	wDay := w.Forecasts[0]
	date := strings.Split(wDay.Date, "-")
	out := "*Погода на " + date[2] + " " + monthsDate[date[1]] + "*\n" +
		icons.Thermometer + " " + strconv.Itoa(wDay.Parts.Day.FeelsLike) + "/" + strconv.Itoa(wDay.Parts.Night.FeelsLike) + " ℃\n" +
		conditions[wDay.Parts.Day.Condition][1] + " " + conditions[wDay.Parts.Day.Condition][0] + "\n" +
		icons.Wind + " " + strconv.Itoa(int(wDay.Parts.Day.WindSpeed)) + " м/с" + "\n"
	return out
}

//Возвращает погоду на 10 дней
func GetWeatherTenDays() []string {
	w, err := getWeather("10")
	if err != nil {
		fmt.Println(err.Error())
		return []string{}
	}
	var outStrings []string

	for _, day := range w.Forecasts {
		date := strings.Split(day.Date, "-")
		out := "*Погода на " + date[2] + " " + monthsDate[date[1]] + "*\n" +
			icons.Thermometer + " " + strconv.Itoa(day.Parts.Day.FeelsLike) + "/" + strconv.Itoa(day.Parts.Night.FeelsLike) + " ℃\n" +
			conditions[day.Parts.Day.Condition][1] + " " + conditions[day.Parts.Day.Condition][0] + "\n" +
			icons.Wind + " " + strconv.Itoa(int(day.Parts.Day.WindSpeed)) + " м/с" + "\n"
		outStrings = append(outStrings, out)
	}

	return outStrings

}
