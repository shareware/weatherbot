package main

import (
	"database/sql"
	"errors"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

const (
	dbConnStr = "bd824e43a74938:028c1d88@/heroku_6514f87752b633d"
)

type dbUser struct {
	chatID       int64
	currentAlarm string
}
type dbAlarm struct {
	ID         int
	userChatID int64
	alarm      string
}

func initDB() {
	db, err := sql.Open("mysql", dbConnStr)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	_, err = db.Exec(`create table IF NOT EXISTS Users(
		ChatId INT UNSIGNED not null UNIQUE primary key,
		CurrentAlarm varchar(5) default ""
	)`)
	if err != nil {
		panic(err.Error())
	}
	_, err = db.Exec(`create table IF NOT EXISTS Alarms(
		Id int auto_increment primary key,
		UserChatId int UNSIGNED,
		foreign key (UserChatId) REFERENCES Users (ChatId) ON DELETE CASCADE,
		Alarm varchar(5) not null
	)`)
	if err != nil {
		panic(err.Error())
	}
}

func getDbConnect() (*sql.DB, error) {
	db, err := sql.Open("mysql", dbConnStr)
	if err != nil {
		return &sql.DB{}, errors.New("Не могу открыть базу")
	}
	return db, nil
}

func initNewUserInDb(chatID int64) error {
	db, err := getDbConnect()
	if err != nil {
		fmt.Println(err.Error())
		db.Close()
	}

	_, err = db.Exec("insert into tgweather.Users (ChatId) values (?)", chatID)
	if err != nil {
		return errors.New("Не могу добавить в базу пользователя")
	}
	_, err = db.Exec("insert into tgweather.Alarms (UserChatId, Alarm) values (?,?)", chatID, "07:00")
	_, err = db.Exec("insert into tgweather.Alarms (UserChatId, Alarm) values (?,?)", chatID, "08:00")
	_, err = db.Exec("insert into tgweather.Alarms (UserChatId, Alarm) values (?,?)", chatID, "09:00")
	if err != nil {
		return errors.New("Не могу добавить в базу пользователя")
	}
	return nil
}

func addUserAlarmToDb(chatID int64, alarm string) error {

	db, err := getDbConnect()
	if err != nil {
		fmt.Println(err.Error())
		db.Close()
	}
	_, err = db.Exec("insert into tgweather.Alarms (UserChatId, Alarm) values (?,?)", chatID, alarm)
	if err != nil {
		return errors.New("Не могу добавить")
	}
	return nil
}

func dbGetCurrentAlarm(chatID int64) (string, error) {

	db, err := getDbConnect()
	if err != nil {
		fmt.Println(err.Error())
		db.Close()
	}

	row := db.QueryRow("select * from tgweather.Users where ChatId = ?", chatID)
	u := dbUser{}
	err = row.Scan(&u.chatID, &u.currentAlarm)
	if err != nil {
		return "", errors.New("Не найден пользователь с таким ID")
	}
	return u.currentAlarm, nil

}

func dbSetCurrentAlarm(chatID int64, alarm string) error {

	db, err := getDbConnect()
	if err != nil {
		fmt.Println(err.Error())
		db.Close()
	}
	_, err = db.Exec("update tgweather.Users set CurrentAlarm = ? where ChatId = ?", alarm, chatID)
	return nil

}

func dbGetAlarms(chatID int64) ([][]string, error) {

	db, err := getDbConnect()
	if err != nil {
		fmt.Println(err.Error())
		db.Close()
	}

	rows, err := db.Query("select * from tgweather.Alarms where UserChatId = ?", chatID)
	if err != nil {
		return nil, errors.New("Не найден пользователь")
	}
	defer rows.Close()
	alarms := [][]string{}
	alarmRow := []string{}
	for rows.Next() {
		al := dbAlarm{}
		err = rows.Scan(&al.ID, &al.userChatID, &al.alarm)
		if err != nil {
			continue
		}

		alarmRow = append(alarmRow, al.alarm)
		if len(alarmRow) == 3 {
			alarms = append(alarms, alarmRow)
			alarmRow = []string{}
		}
	}
	if len(alarmRow) > 0 && len(alarmRow) < 3 {
		alarms = append(alarms, alarmRow)
	}

	return alarms, nil
}

func dbDeleteAlarm(chatID int64, alarm string) error {
	db, err := getDbConnect()
	if err != nil {
		fmt.Println(err.Error())
		db.Close()
	}
	_, err = db.Exec("delete from tgweather.Alarms where UserChatId = ? and Alarm = ?", chatID, alarm)
	return err
}
