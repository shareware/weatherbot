package tgkeyboard

import (
	"github.com/Syfaro/telegram-bot-api"
	"github.com/weathersender/icons"
)

const (
	//WeatherWeek  Текст кнопки На неделю
	WeatherWeek = "На 10 дней " + icons.Calendar
	//WeatherNow Тест кнопки Сейчас
	WeatherNow = "Сейчас " + icons.Thermometer
	//Alarms Текст кнопки Оповещения
	Alarms = "Оповещения " + icons.Alarm
	//NewAlarm Текст кнопки Новое оповещение
	NewAlarm = "Новое оповещение " + icons.Pin
	//Back Текст кнопки Назад
	Back = "Назад " + icons.Back
	//DeleteAlarm Текст кнопки Удалить оповещение
	DeleteAlarm = "Удалить оповещение " + icons.Bucket
)

// GetMainKeyboard Возвращает клавиатуру главного меню
func GetMainKeyboard() tgbotapi.ReplyKeyboardMarkup {

	return newKeyboard([][]string{{WeatherWeek, WeatherNow}, {Alarms}})

}

//GetAlarmKeyboard Возвращает клавиатуру раздела Оповещения
func GetAlarmKeyboard() tgbotapi.ReplyKeyboardMarkup {
	return newKeyboard([][]string{{NewAlarm}, {DeleteAlarm}, {Back}})
}

//GetBackKeyboard Возвращает клавиатуру с нопкой Назад
func GetBackKeyboard() tgbotapi.ReplyKeyboardMarkup {
	return newKeyboard([][]string{{Back}})
}

//GetInlineTimeKeyboard Возвращает клавиатуру оповещений
func GetInlineTimeKeyboard(chekedBtn string, userTime [][]string) tgbotapi.InlineKeyboardMarkup {
	return newInlineKeyboard(userTime, chekedBtn)
}

//newInlineKeyboard создать новую inline клавиатуру
func newInlineKeyboard(buttonNames [][]string, checkedBtn string) tgbotapi.InlineKeyboardMarkup {

	inlineKeyboardBtns := [][]tgbotapi.InlineKeyboardButton{}
	for _, btnLine := range buttonNames {
		lineBtn := []tgbotapi.InlineKeyboardButton{}
		for _, btnName := range btnLine {
			var btnN string
			if checkedBtn != "" && btnName == checkedBtn {
				btnN = btnName + " " + icons.Check
			} else {
				btnN = btnName
			}

			btn := tgbotapi.InlineKeyboardButton{
				Text:         btnN,
				CallbackData: &btnN,
			}
			lineBtn = append(lineBtn, btn)
		}
		inlineKeyboardBtns = append(inlineKeyboardBtns, lineBtn)

	}
	rkm := tgbotapi.InlineKeyboardMarkup{InlineKeyboard: inlineKeyboardBtns}
	return rkm

}

//newKeyboard Создать новую клавиатуру
func newKeyboard(buttonNames [][]string) tgbotapi.ReplyKeyboardMarkup {
	keyboardArrBtns := [][]tgbotapi.KeyboardButton{}
	for _, btnLine := range buttonNames {
		lineBtn := []tgbotapi.KeyboardButton{}
		for _, btn := range btnLine {
			btn := tgbotapi.KeyboardButton{
				Text: btn,
			}
			lineBtn = append(lineBtn, btn)
		}
		keyboardArrBtns = append(keyboardArrBtns, lineBtn)

	}
	rkm := tgbotapi.ReplyKeyboardMarkup{Keyboard: keyboardArrBtns, ResizeKeyboard: true}
	return rkm
}
